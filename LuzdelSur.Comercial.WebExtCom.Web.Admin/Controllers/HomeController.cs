using LuzdelSur.Comercial.WebExtCom.Web.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace LuzdelSur.Comercial.WebExtCom.Web.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            if (!HttpContext.Request.Cookies.ContainsKey("AuthCookie"))
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
